import { analyseText } from "../../resources/calculate-mood-score";
import { AddEntryDeps, Entry, EntryApiData } from "./ports";

export const addNewEntryToProfile =
  (deps: AddEntryDeps) => async (data: Entry & { userId: string }) => {
    const now = new Date();
    const { userId, title, message } = data;

    const toneParams = {
      tone_input: { text: message },
      content_type: "application/json",
    };

    const { mood, stressLevel, count } = analyseText(message);
    const entry: EntryApiData = {
      entity: "Entry",
      created_at: now.toISOString(),
      title,
      message,
      user_id: userId,
      tone: await deps.toneAnalyzer.tone(toneParams),
      mood,
      stressLevel,
      count,
    };

    try {
      await deps.database.addEntry(entry);
      return entry;
    } catch (e) {
      console.log({ e });
      throw new Error(e);
    }
  };
