import { APIGatewayEvent } from 'aws-lambda';
import ToneAnalyzerV3 from 'watson-developer-cloud/tone-analyzer/v3';
import { Database } from '../../resources/dynamodb';

export type Entry = {
  title: string;
  message: string;
};

export type Event = {
  body: Pick<APIGatewayEvent, 'body'> & Entry;
  pathParameters: {
    userId: string;
  };
};

export type EntryApiData = Entry & {
  entity: 'Entry';
  created_at: string;
  user_id: string;
  tone: any;
  mood: number
  stressLevel: number
  count: number
};

export type AddEntryDeps = {
  database: Database
  toneAnalyzer: ToneAnalyzerV3
}
