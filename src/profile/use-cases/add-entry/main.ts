import { APIGatewayProxyResult } from 'aws-lambda';
import ToneAnalyzerV3 from 'watson-developer-cloud/tone-analyzer/v3';
import { commonMiddleware } from '../../../utils/commonMiddleware';
import { AddEntryDeps, Event } from './ports';
import { Database } from '../../resources/dynamodb';
import { addNewEntryToProfile } from '.';

const deps: AddEntryDeps = {
  database: new Database(),
  toneAnalyzer: new ToneAnalyzerV3({
    version: '2017-09-21',
    iam_apikey: process.env.WATSON_TONE || '',
    url: process.env.WATSON_TONE_URL || '',
  }),
};

const addEntry = async (event: Event): Promise<APIGatewayProxyResult> => {
  const { title, message } = event.body;
  const { userId } = event.pathParameters;

  const newEntry = await addNewEntryToProfile(deps)({ userId, title, message });

  return {
    statusCode: 201,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Origin': 'https://ijournal.herokuapp.com',
      'Access-Control-Allow-Methods': 'OPTIONS,POST',
    },
    body: JSON.stringify({
      message: 'User:NewEntry',
      data: newEntry,
    }),
  };
};

export const handler = commonMiddleware(addEntry);
