import { addNewEntryToProfile } from "../index";
import { EntryApiData } from "../ports";

describe("addNewEntryToProfile", () => {
  test("should add new entry to the database", async () => {
    const deps = {
      database: { addEntry: jest.fn().mockResolvedValue("ok") },
      toneAnalyzer: { tone: jest.fn().mockResolvedValue("tone") },
    };
    const data = {
      userId: "123",
      title: "title-test",
      message: "message-test",
    };
    const res = await addNewEntryToProfile(deps as any)(data);

    expect(res).toMatchObject(entry);
  });

  test("should add new entry with tone value", async () => {
    const deps = {
      database: { addEntry: jest.fn().mockResolvedValue("ok") },
      toneAnalyzer: { tone: jest.fn().mockResolvedValue("tone") },
    };
    const data = {
      userId: "123",
      title: "title-test",
      message: "message-test",
    };
    const res = await addNewEntryToProfile(deps as any)(data);

    expect(res).toMatchObject(entry);
    expect(res.tone).toBeDefined();
  });

  test("should throw error when database fails", async () => {
    const deps = {
      database: { addEntry: jest.fn().mockRejectedValue("error-test") },
      toneAnalyzer: { tone: jest.fn().mockResolvedValue("tone") },
    };
    const data = {
      userId: "123",
      title: "title-test",
      message: "message-test",
    };
    const res = addNewEntryToProfile(deps as any)(data);

    expect(res).rejects.toThrow();
  });
});

const entry: EntryApiData = {
  entity: "Entry",
  created_at: expect.any(String),
  title: "title-test",
  message: "message-test",
  user_id: "123",
  tone: "tone",
  mood: 0,
  stressLevel: 0,
  count: 1,
};
