import { Database } from "../../resources/dynamodb";

export type GetProfileDeps = {
  database: Database;
};
