import { APIGatewayProxyResult } from 'aws-lambda';
import { commonMiddleware } from '../../../utils/commonMiddleware';
import { Database } from '../../resources/dynamodb';
import { GetProfileDeps } from './ports';

const deps: GetProfileDeps = {
  database: new Database(),
};

const getProfile = async (event: {
  pathParameters: { userId: string };
}): Promise<APIGatewayProxyResult> => {
  const { userId } = event.pathParameters;

  const entries = await deps.database.getProfile(userId);

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Origin': 'https://ijournal.herokuapp.com',
      'Access-Control-Allow-Methods': 'OPTIONS,GET',
    },
    body: JSON.stringify({
      message: 'User:Entries',
      data: entries.Items,
    }),
  };
};

export const handler = commonMiddleware(getProfile);
