import { analyseText } from "../calculate-mood-score";

describe("calculate-mood-score", () => {
  test("should calculate stressLevel according ith the affin dictionary", () => {
    const testText = "This is a stressed test";
    const res = analyseText(testText);
    expect(res).toHaveProperty("count");
    expect(res).toHaveProperty("mood");
    expect(res).toHaveProperty("stressLevel");
    expect(res.count).toBe(5);
    expect(res.stressLevel).toBe(1);
  });
});
