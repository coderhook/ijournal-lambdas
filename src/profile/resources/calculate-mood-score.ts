import ml from "ml-sentiment";
import * as data from "./afinn_stress.json";

const mlSentiment = ml();

const afinnStress: Record<string, string> = data;

function tokenize(text: string) {
  return text.toLowerCase().split(" ");
}

function deleteChars(word: string) {
  return word.replace(/[^\w]/g, "");
}

function rateWord(word: string) {
  return word in afinnStress ? counter[afinnStress[word]]++ : "";
}

let counter: Record<string, number> = {
  "high stress": 0,
  "partial stress": 0,
  normal: 0,
  happy: 0,
  "very happy": 0,
};

export const analyseText = (text: string) => {
  const moodScore = mlSentiment.classify(text);

  tokenize(text).map(deleteChars).map(rateWord);

  const stressLevel = counter["high stress"] * 2 + counter["partial stress"];
  const result = {
    count: tokenize(text).length,
    mood: moodScore,
    stressLevel,
  };

  // reset the data values
  counter = {
    "high stress": 0,
    "partial stress": 0,
    normal: 0,
    happy: 0,
    "very happy": 0,
  };

  return result;
};
