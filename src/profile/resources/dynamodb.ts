import AWS from "aws-sdk";
import createHttpError from "http-errors";

export class Database {
  private db = new AWS.DynamoDB.DocumentClient();

  public async getProfile(userId: string) {
    const params = {
      TableName: "ProfileTable-dev",
      KeyConditionExpression: "#user_id = :pkey",
      ExpressionAttributeValues: {
        ":pkey": userId,
      },
      ExpressionAttributeNames: {
        "#user_id": "user_id",
      },
      ScanIndexForward: true,
    };

    return await this.db.query(params).promise();
  }

  public async addEntry(entry) {
    try {
      await this.db
        .put({
          TableName: "ProfileTable-dev",
          Item: entry,
        })
        .promise();
    } catch (e) {
      throw new createHttpError.InternalServerError(e);
    }
  }
}
