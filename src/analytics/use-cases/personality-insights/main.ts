import { APIGatewayProxyResult } from "aws-lambda";
import { getPersonalityProfile } from ".";
import { initPersonlityInsights } from "../../../remote/personality-insights";
import { commonMiddleware } from "../../../utils/commonMiddleware";
import { PersonalityInsightEvent, PersonalityInsigthsDeps } from "./ports";

const deps: PersonalityInsigthsDeps = {
  personalityInsights: initPersonlityInsights(),
};

const personalityInsigths = async (
  event: PersonalityInsightEvent
): Promise<APIGatewayProxyResult> => {
  const { data } = event.body;

  const personalityInsights = await getPersonalityProfile(deps)(data);

  return {
    statusCode: 200,
    headers: {
      "Access-Control-Allow-Headers": "Content-Type",
      "Access-Control-Allow-Origin": "https://ijournal.herokuapp.com",
      "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
    },
    body: JSON.stringify(personalityInsights),
  };
};

export const handler = commonMiddleware(personalityInsigths);
