import PersonalityInsightsV3 from 'ibm-watson/personality-insights/v3';

export type PersonalityInsightEvent = {
    body: {
        data: Entry[]
    }
}

export type Entry = {
    created: string
    texts: string
}

export type ContentItems = {
    content: string;
    contenttype: string;
    created: number; // interface explicitly asked for a date in unixTimestamp --> number
    id: string;
    language: string;
}

export type PersonalityInsigthsDeps = {
    personalityInsights: PersonalityInsightsV3
}
