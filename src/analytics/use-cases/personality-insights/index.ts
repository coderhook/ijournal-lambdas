import { ProfileParams } from "ibm-watson/personality-insights/v3";
import { ContentItems, Entry, PersonalityInsigthsDeps } from "./ports";

export const getPersonalityProfile =
  (deps: PersonalityInsigthsDeps) => async (data: any) => {
    const parsedData = mapDataToPersonalityContentItems(data);
    const profileParams: ProfileParams = {
      // Get the content from the JSON file.
      content: { contentItems: parsedData },
      contentType: "application/json",
      consumptionPreferences: true,
      rawScores: true,
    };

    return await deps.personalityInsights
      .profile(profileParams)
      .then((profile) => {
        console.log({ profile });
        return profile;
      })
      .catch((e) => {
        throw new Error(e);
      });
  };

const mapDataToPersonalityContentItems = (data: Entry[]): ContentItems[] => {
  const contentItems = data.map((entry, i: number) => ({
    content: entry.texts,
    contenttype: "text/plain",
    created: Math.round(new Date(entry.created).getTime() / 1000),
    id: String(i),
    language: "en",
  }));
  return contentItems;
};
