import { AnalyseTextDeps } from "./port";

export const analyseText = (deps: AnalyseTextDeps) => async (text: string) => {
  const toneParams = {
    tone_input: { text },
    content_type: "application/json",
  };

  try {
    const result = await deps.toneAnalyzer.tone(toneParams);
    return result;
  } catch (e) {
    throw new Error(e);
  }
};
