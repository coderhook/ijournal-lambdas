import ToneAnalyzerV3 from 'watson-developer-cloud/tone-analyzer/v3';

export type AnalyseTextDeps = {
    toneAnalyzer: ToneAnalyzerV3

}
