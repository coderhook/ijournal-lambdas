import { APIGatewayProxyResult } from 'aws-lambda';
import { analyseText } from '.';
import { initToneAnalyzer } from '../../../remote/tone-analyzer';
import { commonMiddleware } from '../../../utils/commonMiddleware';
import { AnalyseTextDeps } from './port';

const deps: AnalyseTextDeps = {
  toneAnalyzer: initToneAnalyzer(),
};

const analyseTextHandler = async (event: {
  body: { text: string };
}): Promise<APIGatewayProxyResult> => {
  const { text } = event.body;

  const textAnalysis = await analyseText(deps)(text);

  return {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Origin': 'https://ijournal.herokuapp.com',
      'Access-Control-Allow-Methods': 'OPTIONS,POST',
    },
    body: JSON.stringify(textAnalysis),
  };
};

export const handler = commonMiddleware(analyseTextHandler);
