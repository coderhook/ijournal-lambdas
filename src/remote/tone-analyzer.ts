import ToneAnalyzerV3 from 'watson-developer-cloud/tone-analyzer/v3';

export const initToneAnalyzer = () => {
  const options = {
    version: '2017-09-21',
    iam_apikey: process.env.WATSON_TONE || '',
    url: process.env.WATSON_TONE_URL || '',
  };

  return new ToneAnalyzerV3(options);
};
