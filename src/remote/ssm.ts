import AWS from 'aws-sdk';

const ssm = new AWS.SSM({ region: 'eu-west-1' });

const options = {
  Name: '/ijournal-app/dev/watson-tone',
  WithDecryption: true,
};

export const watsonTone = ssm.getParameter(options, (err, data) => console.log(data));
