import PersonalityInsightsV3 from 'ibm-watson/personality-insights/v3';
import { IamAuthenticator } from 'ibm-watson/auth';

const apikey: string = process.env.PERSONALITY_INSIGHTS || '';

export const initPersonlityInsights = () => new PersonalityInsightsV3({
  version: '2017-10-13',
  authenticator: new IamAuthenticator({ apikey }),
  url: process.env.PERSONALITY_INSIGHTS_URL,
});
