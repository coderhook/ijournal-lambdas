"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const commonMiddleware_1 = require("../../../utils/commonMiddleware");
const dynamodb_1 = require("../../resources/dynamodb");
const deps = {
    database: new dynamodb_1.Database()
};
const getProfile = async (event) => {
    const { userId } = event.pathParameters;
    const entries = await deps.database.getProfile(userId);
    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            message: "User:Entries",
            data: entries.Items
        }),
    };
};
exports.handler = commonMiddleware_1.commonMiddleware(getProfile);
//# sourceMappingURL=main.js.map