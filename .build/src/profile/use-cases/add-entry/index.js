"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addNewEntryToProfile = void 0;
const calculate_mood_score_1 = require("../../resources/calculate-mood-score");
const addNewEntryToProfile = (deps) => async (data) => {
    const now = new Date();
    const { userId, title, message } = data;
    const toneParams = {
        tone_input: { text: message },
        content_type: "application/json",
    };
    const { mood, stressLevel, count } = calculate_mood_score_1.analyseText(message);
    const entry = {
        entity: "Entry",
        created_at: now.toISOString(),
        title,
        message,
        user_id: userId,
        tone: await deps.toneAnalyzer.tone(toneParams),
        mood,
        stressLevel,
        count
    };
    await deps.database.addEntry(entry);
    return entry;
};
exports.addNewEntryToProfile = addNewEntryToProfile;
//# sourceMappingURL=index.js.map