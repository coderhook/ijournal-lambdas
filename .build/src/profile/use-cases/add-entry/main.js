"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const commonMiddleware_1 = require("../../../utils/commonMiddleware");
const v3_1 = __importDefault(require("watson-developer-cloud/tone-analyzer/v3"));
const dynamodb_1 = require("../../resources/dynamodb");
const _1 = require(".");
const deps = {
    database: new dynamodb_1.Database(),
    toneAnalyzer: new v3_1.default({
        version: "2017-09-21",
        iam_apikey: process.env.WATSON_TONE || "",
        url: process.env.WATSON_TONE_URL || "",
    })
};
const addEntry = async (event) => {
    const { title, message } = event.body;
    const { userId } = event.pathParameters;
    const newEntry = await _1.addNewEntryToProfile(deps)({ userId, title, message });
    return {
        statusCode: 201,
        body: JSON.stringify({
            message: 'User:NewEntry',
            data: newEntry
        }),
    };
};
exports.handler = commonMiddleware_1.commonMiddleware(addEntry);
//# sourceMappingURL=main.js.map