"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.analyseText = void 0;
const ml_sentiment_1 = __importDefault(require("ml-sentiment"));
const data = __importStar(require("./afinn_stress.json"));
const mlSentiment = ml_sentiment_1.default();
const afinnStress = data;
function tokenize(text) {
    return text
        .toLowerCase()
        .split(' ');
}
function deleteChars(word) {
    return word.replace(/[^\w]/g, '');
}
function rateWord(word) {
    return (word in afinnStress) ? counter[afinnStress[word]]++ : '';
}
let counter = {
    'high stress': 0,
    'partial stress': 0,
    'normal': 0,
    'happy': 0,
    'very happy': 0
};
const analyseText = (text) => {
    const moodScore = mlSentiment.classify(text);
    tokenize(text)
        .map(deleteChars)
        .map(rateWord);
    const stressLevel = counter['high stress'] * 2 + counter['partial stress'];
    const result = {
        count: tokenize(text).length,
        mood: moodScore,
        stressLevel
    };
    // reset the data values
    counter = {
        'high stress': 0,
        'partial stress': 0,
        'normal': 0,
        'happy': 0,
        'very happy': 0
    };
    return result;
};
exports.analyseText = analyseText;
//# sourceMappingURL=calculate-mood-score.js.map