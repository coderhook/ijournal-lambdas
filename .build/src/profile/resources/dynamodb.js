"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const http_errors_1 = __importDefault(require("http-errors"));
class Database {
    constructor() {
        this.db = new aws_sdk_1.default.DynamoDB.DocumentClient();
    }
    async getProfile(userId) {
        const params = {
            "TableName": "ProfileTable-dev",
            "KeyConditionExpression": "#user_id = :pkey",
            "ExpressionAttributeValues": {
                ":pkey": userId
            },
            "ExpressionAttributeNames": {
                "#user_id": "user_id"
            },
            "ScanIndexForward": true
        };
        return await this.db.query(params).promise();
    }
    async addEntry(entry) {
        try {
            await this.db
                .put({
                TableName: "ProfileTable-dev",
                Item: entry,
            })
                .promise();
        }
        catch (e) {
            throw new http_errors_1.default.InternalServerError(e);
        }
    }
}
exports.Database = Database;
//# sourceMappingURL=dynamodb.js.map