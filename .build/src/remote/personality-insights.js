"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initPersonlityInsights = void 0;
const v3_1 = __importDefault(require("ibm-watson/personality-insights/v3"));
const auth_1 = require("ibm-watson/auth");
const apikey = process.env.PERSONALITY_INSIGHTS || "";
const initPersonlityInsights = () => {
    return new v3_1.default({
        version: "2017-10-13",
        authenticator: new auth_1.IamAuthenticator({ apikey }),
        url: process.env.PERSONALITY_INSIGHTS_URL,
    });
};
exports.initPersonlityInsights = initPersonlityInsights;
//# sourceMappingURL=personality-insights.js.map