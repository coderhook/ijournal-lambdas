"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initToneAnalyzer = void 0;
const v3_1 = __importDefault(require("watson-developer-cloud/tone-analyzer/v3"));
const initToneAnalyzer = () => {
    const options = {
        version: "2017-09-21",
        iam_apikey: process.env.WATSON_TONE || "",
        url: process.env.WATSON_TONE_URL || "",
    };
    return new v3_1.default(options);
};
exports.initToneAnalyzer = initToneAnalyzer;
//# sourceMappingURL=tone-analyzer.js.map