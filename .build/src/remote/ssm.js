"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.watsonTone = void 0;
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const ssm = new aws_sdk_1.default.SSM({ region: "eu-west-1" });
const options = {
    Name: "/ijournal-app/dev/watson-tone",
    WithDecryption: true,
};
exports.watsonTone = ssm.getParameter(options, (err, data) => console.log(data));
//# sourceMappingURL=ssm.js.map