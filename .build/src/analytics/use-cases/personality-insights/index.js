"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPersonalityProfile = void 0;
const getPersonalityProfile = (deps) => async (data) => {
    const parsedData = mapDataToPersonalityContentItems(data);
    const profileParams = {
        // Get the content from the JSON file.
        content: { contentItems: parsedData },
        contentType: "application/json",
        consumptionPreferences: true,
        rawScores: true,
    };
    return await deps.personalityInsights
        .profile(profileParams)
        .then((profile) => {
        console.log({ profile });
        return profile;
    })
        .catch((err) => console.log("error n pers", err));
};
exports.getPersonalityProfile = getPersonalityProfile;
const mapDataToPersonalityContentItems = (data) => {
    const contentItems = data.map((entry, i) => ({
        "content": entry.texts,
        "contenttype": "text/plain",
        "created": Math.round(new Date(entry.created).getTime() / 1000),
        "id": String(i),
        "language": "en"
    }));
    return contentItems;
};
//# sourceMappingURL=index.js.map