"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const _1 = require(".");
const personality_insights_1 = require("../../../remote/personality-insights");
const commonMiddleware_1 = require("../../../utils/commonMiddleware");
const deps = {
    personalityInsights: personality_insights_1.initPersonlityInsights()
};
const personalityInsigths = async (event) => {
    const { data } = event.body;
    const personalityInsights = await _1.getPersonalityProfile(deps)(data);
    console.log({ personalityInsights });
    return {
        statusCode: 200,
        body: JSON.stringify(personalityInsights)
    };
};
exports.handler = commonMiddleware_1.commonMiddleware(personalityInsigths);
//# sourceMappingURL=main.js.map