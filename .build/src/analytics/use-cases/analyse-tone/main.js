"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const _1 = require(".");
const tone_analyzer_1 = require("../../../remote/tone-analyzer");
const commonMiddleware_1 = require("../../../utils/commonMiddleware");
const deps = {
    toneAnalyzer: tone_analyzer_1.initToneAnalyzer()
};
const analyseTextHandler = async (event) => {
    const { text } = event.body;
    const textAnalysis = await _1.analyseText(deps)(text);
    console.log({ textAnalysis });
    return {
        statusCode: 200,
        body: JSON.stringify(textAnalysis)
    };
};
exports.handler = commonMiddleware_1.commonMiddleware(analyseTextHandler);
//# sourceMappingURL=main.js.map