"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.analyseText = void 0;
const analyseText = (deps) => async (text) => {
    const toneParams = {
        tone_input: { text },
        content_type: "application/json",
    };
    try {
        const result = await deps.toneAnalyzer.tone(toneParams);
        return result;
    }
    catch (e) {
        console.log({ e });
        throw new Error(e);
    }
};
exports.analyseText = analyseText;
//# sourceMappingURL=index.js.map