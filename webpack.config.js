/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')
const nodeExternals = require('webpack-node-externals')
const slsw = require('serverless-webpack')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

const isLocal = slsw.lib.webpack.isLocal

module.exports = {
  mode: isLocal ? 'development' : 'production',
  entry: slsw.lib.entries,
  devtool: 'source-map',
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
  },
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js',
  },
  externals: [nodeExternals()],
  target: 'node',
  module: {
    rules: [
      {
        test: /\.(ts|js)x?$/,
        loader: 'ts-loader',
        options: {transpileOnly: true, configFile: 'tsconfig.json'},
        exclude: /node_modules/,
        // use: [
        //   {
        //     loader: 'cache-loader',
        //     options: {
        //       cacheDirectory: path.resolve('.webpackCache'),
        //     },
        //   },
        //   'babel-loader',
        // ],
      },
    ],
  },
  plugins: [new ForkTsCheckerWebpackPlugin()],
}
