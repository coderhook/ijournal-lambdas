# Lambda Serverless ShoeStore

## Service Information

- Function names exposed for local invocation by aws-sdk:
  _ get-profile: lambda-ijournal-analyze-dev-get-profile
  _ add-entry: lambda-ijournal-analyze-dev-add-entry
  _ analyse-tone: lambda-ijournal-analyze-dev-analyse-tone
  _ personality-insights: lambda-ijournal-analyze-dev-personality-insights

# Base Serverless Framework Template

## What's included

- Folder structure.
- [serverless-pseudo-parameters plugin](https://www.npmjs.com/package/serverless-pseudo-parameters):
  Allows you to take advantage of CloudFormation Pseudo Parameters.
- [serverless-bundle plugin](https://www.npmjs.com/package/serverless-pseudo-parameters):
  Bundler based on the serverless-webpack plugin - requires zero configuration
  and fully compatible with ES6/ES7 features.
-
